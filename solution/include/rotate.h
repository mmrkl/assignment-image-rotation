#include "image.h"

typedef image (*transformer) (image const, image);

image rotate( image const source, image dst );

image transform(image const source, transformer t);
