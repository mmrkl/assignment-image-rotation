#include "../include/bmp.h"
#include "../include/rotate.h"
#include "../include/util.h"

const uint8_t ARG_COUNT = 3;

void usage() {
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>\n"); 
}

int main( int argc, char** argv ) {
    if (argc != ARG_COUNT) usage();
    if (argc < ARG_COUNT) err("Not enough arguments \n" );
    if (argc > ARG_COUNT) err("Too many arguments \n" );
    

    image in = read_file(argv[1]);
    image out = transform(in, rotate);
    write_file(argv[2], out);
    
    image_clear(&in);
    image_clear(&out);

    return 0;
}
