#include "../include/rotate.h"
#include <stdio.h> 

/*
data: 1 2 3 4 5 6 7 8 9 a b c

1 2 3
4 5 6
7 8 9
a b c

rotate =>  

a 7 4 1
b 8 5 2
c 9 6 3

write to file =>

3 6 9 c
2 5 8 b
1 4 7 a
*/

pixel get_pixel(image const source, uint64_t i) {
    return source.data[i];
}

image rotate( image const source, image dst ) {
    for (uint64_t i = 0, k = 0; i < source.width; ++i) {
        for (uint64_t j = source.height; j > 0 ; --j) {
            dst.data[k++] = get_pixel(source, (j - 1) * source.width + i);
        }
    }
    return dst;
}

image transform(image const source, transformer t) {
    image dst = {0};
    image_init(&dst, source.height, source.width);
    return t(source, dst);
}
